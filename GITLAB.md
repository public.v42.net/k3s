# GitLab Kubernetes integration

GitLab provides [full documentation](https://docs.gitlab.com/ee/user/project/clusters/index.html) about Kubernetes integration.
Kubernetes (so also `k3s`) can be integrated at the GitLab [project](https://docs.gitlab.com/ee/user/project/clusters/index.html), [group](https://docs.gitlab.com/ee/user/group/clusters/index.html) or [instance](https://docs.gitlab.com/ee/user/instance/clusters/index.html) level. For this exercise I've chosen to integrate my `k3s` at the GitLab instance level of my own GitLab instance, using the procedure to [add an existing cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) (or single node).

## Allow GitLab to access `k3s`

To be able to use GitLab's Kubernetes integration, GitLab needs to allow web hooks and services to access the `k3s` node(s):

- GitLab Admin Area => Settings
    - Network => Outbound Requests
        - Local IP addresses and domain names that hooks and services may access

Add the address(es) of the `k3s` node(s) and save the changes.

## Allow `k3s` access from GitLab

To allow my GitLab instance at `192.168.1.10` to access the `k3s` API, the following `iptables` rule is needed in [`/etc/iptables/rules.v4`](iptables/rules.v4)

```
-A INPUT -s 192.168.1.10/32 -p tdp --dport 6443 -j ACCEPT
```

As `k3s` is already running and has created a lot of `iptables` rules, do *not* load the modified rules with `iptables-restore`. 
Either reboot the `k3s` node or add the new rule manually.

## Collect all required information

You might want to open a blank document in your favorite editor to record the information:

- The API URL
- The CA certificate
- The API token

This information will be needed when [configuring the GitLab Kubernetes integration](#configure-gitlab-kubernetes-integration).

### The API URL

The [documented procedure to retrieve the API URL](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) (at bullet `3c`) did not work for me: it returned `https://127.0.0.1:6443`. So I used the GitLab system to determine the API URL.
On the GitLab system use the following command to display the API certificate:
```
echo | openssl s_client -showcerts -connect 192.168.1.30:6443 2>/dev/null | openssl x509 -inform pem -noout -text
```
Note the `X509v3 Subject Alternative Name` section:
```
 X509v3 Subject Alternative Name:
                DNS:kubernetes, DNS:kubernetes.default, DNS:kubernetes.default.svc.kube.v42.net, DNS:localhost, IP Address:10.43.0.1, IP Address:127.0.0.1, IP Address:198.168.1.30
```
To be able to connect to the API we'll need to use a URL based on one of these 'Subject Alternative Names'. 
When using any other name or address, GitLab will not be able to verify the certificate as the name (or address) does not match.
Record the URL you will be using to access the `k3s` API. For instance:
```
https://198.168.1.30:6443/
```
With `curl -k https://192.168.1.30:6443/` you should be able to get a JSON formatted response from the `k3s` API.

### The CA certificate

Use the [documented procedure to retrieve the CA certificate](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) (at bullet `3d`) and record this CA certificate. For instance:
```
-----BEGIN CERTIFICATE-----
MIIBdzCCAR2gAwIBAgIBADAKBggqhkjOPQQDAjAjMSEwHwYDVQQDDBhrM3Mtc2Vy
dmVyLWNhQDE2MTk5ODE3MjcwHhcNMjEwNTAyMTg1NTI3WhcNMzEwNDMwMTg1NTI3
WjAjMSEwHwYDVQQDDBhrM3Mtc2VydmVyLWNhQDE2MTk5ODE3MjcwWTATBgcqhkjO
PQIBBggqhkjOPQMBBwNCAARsAsgEV12CcjOTemk340qMDkfKINLy4ZeHaEbgis+S
3+j0Jm1OCuFN6kDKjtM4BjpNa+ILLBKXdKU492/cPErko0IwQDAOBgNVHQ8BAf8E
BAMCAqQwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUXxt4PRY5SyZE+IqVJqUX
RmpogcQwCgYIKoZIzj0EAwIDSAAwRQIgXbG+AqRitG0h8YEzkDGCxhglShUyABD9
I/ZlIIe29HoCIQDj1TadPs19287glVQiMbME55XmWkj08revOmJZ6DmTRw==
-----END CERTIFICATE-----
```

### The API token
Use the [documented procedure to create the gitlab service account and retrieve the token](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) (at bullet `3e`) and record this token. For instance:
```
eyJhbGciOiJSUzI1NiIsImtpZCI6Imxha0NkWHJCNndYeklIVmZjamhOaE93WUVIc1k2MnJGanJPRmtoNEI4cmcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJnaXRsYWItdG9rZW4tbnI2ZnAiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZ2l0bGFiIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiZDUyOTQ4MTctODdmZi00YzgyLWE2NzctYThhNzE1ZDA1ZDUyIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmdpdGxhYiJ9.fhwi7yc8PJ8UK3GitVA1AIP_KRHhypOuVC7Qyw95XTjC9a6O9BxnOsAy_GOX-PxQDIs29lcY3RQv3ifd-RXb-9TwUf9GzTJBD5_D7zYkY7IRxHo05HCgufLPWClM0he4J2vcObl24FyS618SlNjnWaqb_CHY3rLVrQ4u67AweX5qYvMo-EVlY9YvTj-gMlu4RY9KfFnJPUV2CCSVKJX_tV5ixoVlXY_v1k0qdhQpZJQdvOyagQlSiV7zQE_Rq_Mq7nkqA4RCpo2J9BZQKL0RQLhyzzH2TT0pYlHCOSRgffn4Q8A4On8YgvTMpfFmNl2F1nlYIyA-Uvm8Wkxrs9vGcA
```

## Configure GitLab Kubernetes integration

Now use the [documented procedure](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) to integrate GitLab with `k3s`, using the previously recorded information:

- GitLab Admin Area => Kubernetes 
  - Integrate with a cluster certificate => Connect Existing cluster
    - Kubernetes cluster name = kube
    - Environment scope = kube
    - API URL = `<as recorded previously>`
    - CA certificate = `<as recorded previously>`
    - Service Token = `<as recorded previously>`
    - [x] RBAC-enabled cluster
    - [x] GitLab-managed cluster
    - [x] Namespace per Environment

On successful completion, select the applications tab and install GitLab Runner. Be careful before installing other applications: `k3s` already has an `Ingress` installed, as well as some other applications. Thoroughly check the [`k3s` documentation](https://rancher.com/docs/k3s/latest/en/) before installing any other applciations: first make sure that they are compatible with `k3s`.

## References

- [Using Docker in your builds: there are a couple of caveats ...](https://docs.gitlab.com/runner/executors/kubernetes.html#using-docker-in-your-builds)

