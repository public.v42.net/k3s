# Setting up a `k3s` agent

- [Install a virtual machine](#install-a-virtual-machine)
- [Configure `iptables` ...](#configure-iptables-)
- [Install `k3s` agent](#install-k3s-agent)

## Install a virtual machine

I used a virtual system with the following specifications, using the latest version of Debian:

| Hostname | CPU's | Memory | Disk | Operating System | IP Address | gateway |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| kube | 1 | 2 GB | 32 GB | Debian 10.9 | 192.168.1.34/26 | 192.168.1.1 |

During the installation I removed the swap partition and only selected the `SSH server` and the `standard system utilities`. To be able to manage the system and install `k3s`, I additionally installed `curl` and `net-tools`. To secure the system I disabled SSH password authentication: the only way to remotely access this system is via the console or with an SSH key.

## Configure `iptables` ...

Due to initial problems with `iptables` I configured the rules I determined when [setting up a `k3s` server](K3S-SERVER.md#configure-iptables-). [Read more ...](IPTABLES.md)

## Install `k3s` agent

To install `k3s` agent and connect it to my [`k3s` server](K3S-SERVER.md) I would use the following command:
```
curl -sfL https://get.k3s.io | sh -s - agent --server https://192.168.1.30:6443 \
                                             --token "<node-token>"
```
To install `k3s` agent and connect it to my [`k3s` cluster](K3S-CLUSTER.md) I would use the following command:
```
curl -sfL https://get.k3s.io | sh -s - agent --server https://192.168.1.31:6443 \
                                             --token "<node-token>"
```
After allowing the `k3s` agent some time to get started, the result can be checked on the *server* or *cluster node*:
```
# kubectl get nodes
NAME    STATUS   ROLES                       AGE   VERSION
kube1   Ready    control-plane,etcd,master   25m   v1.20.6+k3s1
kube2   Ready    control-plane,etcd,master   22m   v1.20.6+k3s1
kube3   Ready    control-plane,etcd,master   18m   v1.20.6+k3s1
kube4   Ready    <none>                      14m   v1.20.6+k3s1

# kubectl get pods -o wide -A
NAMESPACE     NAME                                      READY   STATUS      RESTARTS   AGE   IP          NODE    NOMINATED NODE   READINESS GATES
kube-system   coredns-854c77959c-78wq6                  1/1     Running     0          33m   10.42.0.3   kube1   <none>           <none>
kube-system   helm-install-traefik-q5p4b                0/1     Completed   0          33m   10.42.0.4   kube1   <none>           <none>
kube-system   local-path-provisioner-5ff76fc89d-nqrvn   1/1     Running     0          33m   10.42.0.2   kube1   <none>           <none>
kube-system   metrics-server-86cbb8457f-l7wnv           1/1     Running     0          33m   10.42.0.5   kube1   <none>           <none>
kube-system   svclb-traefik-hlr27                       2/2     Running     0          33m   10.42.0.6   kube1   <none>           <none>
kube-system   svclb-traefik-lbpxc                       2/2     Running     0          26m   10.42.2.2   kube3   <none>           <none>
kube-system   svclb-traefik-p9hwg                       2/2     Running     0          22m   10.42.3.2   kube4   <none>           <none>
kube-system   svclb-traefik-zqzlt                       2/2     Running     0          29m   10.42.1.2   kube2   <none>           <none>
kube-system   traefik-6f9cbd9bd4-c26kf                  1/1     Running     0          33m   10.42.0.7   kube1   <none>           <none>
```
To be able to use `kubectl` on the `k3s` agent system it needs to be configured:
- Copy `/etc/rancher/k3s/k3s.yaml` from the *server* or *cluster node* to the *agent* system.
- Edit this file on the *agent* system to connect to *server* or a *cluster node* instead of ``127.0.0.1`.

Now `kubectl` on the *agent* system is able to access the `k3s` *server* or *cluster* environment.


