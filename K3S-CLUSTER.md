# Setting up a `k3s` cluster

- [Install three virtual machines](#install-three-virtual-machines)
- [Configure `iptables` ...](#configure-iptables-)
- [Install `k3s` cluster](#install-k3s-cluster)

## Install three virtual machines

I used three virtual systems with the following specifications, using the latest version of Debian:

| Hostname | CPU's | Memory | Disk | Operating System | IP Address | gateway |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| kube1 | 1 | 2 GB | 32 GB | Debian 10.9 | 192.168.1.31/26 | 192.168.1.1 |
| kube2 | 1 | 2 GB | 32 GB | Debian 10.9 | 192.168.1.32/26 | 192.168.1.1 |
| kube3 | 1 | 2 GB | 32 GB | Debian 10.9 | 192.168.1.33/26 | 192.168.1.1 |

During the installation I removed the swap partition and only selected the `SSH server` and the `standard system utilities`. To be able to manage the system and install `k3s`, I additionally installed `curl` and `net-tools`. To secure the system I disabled SSH password authentication: the only way to remotely access these three systems is via the console or with an SSH key.

## Configure `iptables` ...

Due to initial problems with `iptables` I configured the rules I determined when [setting up a `k3s` server](K3S-SERVER.md#configure-iptables-). [Read more ...](IPTABLES.md)

## Initialize the `k3s` cluster

To install `k3s` on the *first* cluster node I used the following command:
```
curl -sfL https://get.k3s.io | sh -s - --cluster-init                               \
                                       --write-kubeconfig-mode  644                 \
                                       --cluster-cidr           10.42.0.0/16        \
                                       --service-cidr           10.43.0.0/16        \
                                       --cluster-dns            10.43.0.10          \
                                       --cluster-domain         kubes.example.com    \
                                       --tls-san                kubes.example.com
```
Then I allowed `k3s` some time to get started before checking the result:
```
# kubectl get nodes
NAME    STATUS   ROLES                       AGE   VERSION
kube1   Ready    control-plane,etcd,master   2m    v1.20.6+k3s1

# kubectl get pods -A
NAMESPACE     NAME                                      READY   STATUS      RESTARTS   AGE
kube-system   coredns-854c77959c-p989w                  1/1     Running     0          113s
kube-system   helm-install-traefik-c9ztf                0/1     Completed   0          114s
kube-system   local-path-provisioner-5ff76fc89d-k2fxn   1/1     Running     0          113s
kube-system   metrics-server-86cbb8457f-6kp82           1/1     Running     0          113s
kube-system   svclb-traefik-pxb7k                       2/2     Running     0          91s
kube-system   traefik-6f9cbd9bd4-kghnj                  1/1     Running     0          91s

# kubectl get svc -A
NAMESPACE     NAME                 TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)                      AGE
default       kubernetes           ClusterIP      10.43.0.1      <none>         443/TCP                      2m13s
kube-system   kube-dns             ClusterIP      10.43.0.10     <none>         53/UDP,53/TCP,9153/TCP       2m11s
kube-system   metrics-server       ClusterIP      10.43.182.55   <none>         443/TCP                      2m10s
kube-system   traefik              LoadBalancer   10.43.87.110   192.168.1.31   80:32617/TCP,443:31419/TCP   95s
kube-system   traefik-prometheus   ClusterIP      10.43.20.213   <none>         9100/TCP                     95s

# kubectl get deployments -A
NAMESPACE     NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   coredns                  1/1     1            1           2m16s
kube-system   local-path-provisioner   1/1     1            1           2m16s
kube-system   metrics-server           1/1     1            1           2m15s
kube-system   traefik                  1/1     1            1           100s
```

## Install the *second* and *third* node

To install `k3s` on the *remaining* cluster nodes get the `<node-token>` from the *first* cluster node:
```
# cat /var/lib/rancher/k3s/server/node-token
K10ddaca448429c83285490868f516880a383950348b7a267c7c8f239faafd99438::server:a86b691830ee48ac86bb5d87b1995e11
```
The `<node-token>` is required to install `k3s` on the *second* and *third* node with the following command:
```
curl -sfL https://get.k3s.io | sh -s - server --server https://192.168.1.31:6443 \
                                              --token "<node-token>" \
                                              --write-kubeconfig-mode 644
```
Then I allowed `k3s` on both new nodes some time to get started before checking the result:
```
# kubectl get nodes
NAME    STATUS   ROLES                       AGE     VERSION
kube1   Ready    control-plane,etcd,master   18m     v1.20.6+k3s1
kube2   Ready    control-plane,etcd,master   11m     v1.20.6+k3s1
kube3   Ready    control-plane,etcd,master   2m12s   v1.20.6+k3s1

# kubectl get pods -A
NAMESPACE     NAME                                      READY   STATUS      RESTARTS   AGE
kube-system   coredns-854c77959c-p989w                  1/1     Running     0          18m
kube-system   helm-install-traefik-c9ztf                0/1     Completed   0          18m
kube-system   local-path-provisioner-5ff76fc89d-k2fxn   1/1     Running     0          18m
kube-system   metrics-server-86cbb8457f-6kp82           1/1     Running     0          18m
kube-system   svclb-traefik-9k7sx                       2/2     Running     0          11m
kube-system   svclb-traefik-j4kvh                       2/2     Running     0          2m6s
kube-system   svclb-traefik-pxb7k                       2/2     Running     0          17m
kube-system   traefik-6f9cbd9bd4-kghnj                  1/1     Running     0          17m

# kubectl get svc -A
NAMESPACE     NAME                 TYPE           CLUSTER-IP     EXTERNAL-IP                              PORT(S)                      AGE
default       kubernetes           ClusterIP      10.43.0.1      <none>                                   443/TCP                      18m
kube-system   kube-dns             ClusterIP      10.43.0.10     <none>                                   53/UDP,53/TCP,9153/TCP       18m
kube-system   metrics-server       ClusterIP      10.43.182.55   <none>                                   443/TCP                      18m
kube-system   traefik              LoadBalancer   10.43.87.110   192.168.1.31,192.168.1.32,192.168.1.33   80:32617/TCP,443:31419/TCP   17m
kube-system   traefik-prometheus   ClusterIP      10.43.20.213   <none>                                   9100/TCP                     17m

# kubectl get deployments -A
NAMESPACE     NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   coredns                  1/1     1            1           18m
kube-system   local-path-provisioner   1/1     1            1           18m
kube-system   metrics-server           1/1     1            1           18m
kube-system   traefik                  1/1     1            1           18m
```
